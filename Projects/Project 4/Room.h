#ifndef _ROOM_H
#define _ROOM_H

#include <string>
using namespace std;

struct Room
{
	Room();
	void Display();

	string m_name;
	string m_description;

	Room* m_ptrNeighborWest;
	Room* m_ptrNeighborEast;
	Room* m_ptrNeighborNorth;
	Room* m_ptrNeighborSouth;

};

#endif
