#ifndef _TEXT_ADVENTURE_H
#define _TEXT_ADVENTURE_H

#include "Room.h"

class TextAdventure
{
public:
	TextAdventure();
	~TextAdventure();
	void Run();

private:
	bool m_isDone;
	Room* m_rooms;
	int m_totalRooms; // dynamic array
	Room* m_ptrCurrentRoom;


	// memory management
	void AllocateSpace(int size);
	void DeallocateSpace();

	// file loading
	int GetIndexOfRoomWithName(string name);
	void LoadGameData();

	// game loop function
	string GetUserCommand();
	void TryToMove(string command);
};

#endif