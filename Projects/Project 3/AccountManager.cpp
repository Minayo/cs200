#include "AccountManager.hpp"

#include <iostream>
using namespace std;

AccountManager::AccountManager()
{
    m_loggedInUser = -1;
    Load();
}

AccountManager::~AccountManager()
{
    Save();
}

void AccountManager::AddAccount( string username, string plaintextPassword )
{
    if (UsernameExists(username))
    {
        cout << "Username already taken." << endl;
        return;
    }

    size_t hashedPassword = HashPassword(plaintextPassword);

    Account newAccount;
    newAccount.Setup(username, hashedPassword);
    m_accounts.push_back(newAccount);
}

bool AccountManager::UsernameExists( string username ) const
{
    for (auto& account : m_accounts)
    {
        string exist = account.GetUsername();
        if (username == exist)
        {
            return true;
        }

    }
    return false;
}

bool AccountManager::ValidatePassword( string username, string plaintextPassword )
{
    size_t hashedPassword = HashPassword(plaintextPassword);

    for (auto& account : m_accounts)
    {
        if (username == account.GetUsername())
        {
            if (account.ValidatePassword(hashedPassword) == true)
            {
                return true;
            }
        }
    }

    return false; 
}

int AccountManager::GetIndexOfUser( string username )
{

    for (unsigned int i = 0; i < m_accounts.size(); i++)
    {
        if (username == m_accounts[i].GetUsername())
        {
            return i;
        }

    }
    return -1; 
}

bool AccountManager::SignIn( string username, string plaintextPassword )
{
    if (ValidatePassword(username, plaintextPassword) == false)
    {
        return false;
    }
    else
    {
        m_loggedInUser = GetIndexOfUser(username);
        return true;
    }
     
}

string AccountManager::GetLoggedInUsername() const
{
    if (m_loggedInUser == -1)
    {
        return "";
    }
    else
    {
        return m_accounts[m_loggedInUser].GetUsername();
    }
}


/* Already written functions **********************************************/
void AccountManager::Load()
{
    ifstream input( "accounts.dat" );
    if ( input.fail() )
    {
        return;
    }

    string username;
    size_t password;

    while ( input >> username >> password )
    {
        Account account( username, password );
        m_accounts.push_back( account );
    }

    cout << m_accounts.size() << " accounts loaded." << endl;
}

void AccountManager::Save()
{
    ofstream output( "accounts.dat" );
    for ( auto& account : m_accounts )
    {
        output << account.GetUsername() << " " << account.GetHashedPassword() << endl;
    }
    output.close();
}

size_t AccountManager::HashPassword( string plaintextPassword )
{
    hash<string> stringHash;
    return stringHash( plaintextPassword );
}
