#include "Account.hpp"

Account::Account()
{
}

Account::Account( string username, size_t hashedPassword )
{
    Setup(username, hashedPassword);
}

void Account::Setup( string username, size_t hashedPassword )
{
    m_username = username;
    m_hashedPassword = hashedPassword;
}

bool Account::ValidatePassword( size_t hashedPasswordAttempt ) const
{
    if (hashedPasswordAttempt == m_hashedPassword)
    {
        return true;
    }
    else
    {
        return false;
    }
}

string Account::GetUsername() const
{
    return m_username; 
}

size_t Account::GetHashedPassword() const
{
    return m_hashedPassword; 
}
