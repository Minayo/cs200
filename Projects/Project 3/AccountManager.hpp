#ifndef _ACCOUNT_MANAGER_HPP
#define _ACCOUNT_MANAGER_HPP

#include "Account.hpp"

#include <fstream>
#include <vector>
using namespace std;

class AccountManager
{
    public:
    AccountManager();
    ~AccountManager();

    bool UsernameExists( string username ) const;
    void AddAccount( string username, string plaintextPassword );
    bool SignIn( string username, string plaintextPassword );
    string GetLoggedInUsername() const;

    private:
    vector<Account> m_accounts;
    int m_loggedInUser;

    void Load();
    void Save();
    size_t HashPassword( string plaintextPassword );
    bool ValidatePassword( string username, string plaintextPassword );
    int GetIndexOfUser( string username );
};

#endif
