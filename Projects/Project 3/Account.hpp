#ifndef _ACCOUNT_HPP
#define _ACCOUNT_HPP

#include <functional>
#include <string>
using namespace std;

class Account
{
    public:
    Account();
    Account( string username, size_t hashedPassword );

    void Setup( string username, size_t hashedPassword );
    bool ValidatePassword( size_t hashedPasswordAttempt ) const;
    string GetUsername()  const;
    size_t GetHashedPassword()  const;

    private:
    string m_username;
    size_t m_hashedPassword;
};

#endif
