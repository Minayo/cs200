#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP

#include <string>
using namespace std;

void DisplayMainMenu( int savedMovies );
int GetChoice( int min, int max );
void ClearScreen();
void Pause();

void AddMovie( string movieList[], int & savedMovies, const int MAX_MOVIES );
void UpdateMovie( string movieList[], int savedMovies );
void ClearAllMovies( string movieList[], int & savedMovies, const int MAX_MOVIES );
void ViewAllMovies( string movieList[], int savedMovies );
void SaveMovies( string movieList[], int savedMovies );
void LoadMovies( string movieList[], int & savedMovies );

#endif
