#include <iostream>
#include <string>
#include "card.h"

int main()
{
	const int SUITS = 4;
	const int RANKS = 13;
	Card cards[SUITS][RANKS];
	
	const char suits[] = { 'C', 'D', 'H', 'S' };
	const string ranks[] = { "A", "2", "3", "4",
							"5", "6", "7","8","9",
							"10","J","Q","K" };

	for (int s = 0; s < SUITS; s++)
	{
		for (int r = 0; r < RANKS; r++)
		{
			char suit = suits[s];
			string rank = ranks[r];
			cards[s][r].Setup(suit, rank);
		}
	}

	cout << "Enter a number 0-3 for suit: ";
	int userSuit;
	cin >> userSuit;
	cout << "ENter a number 0-12 for rank: ";
	int userRank;
	cin >> userRank;

	int compSuit = rand() % 3;
	int compRank = rand() % 12;

	cout << endl << "You pick ";
	cards[userSuit][userRank].Display();
	cout << "Computer pick ";
	cards[compSuit][compRank].Display();


	return 0;
}