#ifndef _CARD_HPP
#define _CARD_HPP
#include <string>
using namespace std;

class Card
{
public:
	void Setup(char newSuit, string newRank);
	void Display();
	void DisplayName();
	string GetRank();
	char GetSuit();

private:
	string rank;
	char suit; 
};

#endif