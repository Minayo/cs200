#include "card.h"
#include <iostream>
#include <string>
using namespace std;

void Card::Setup(char newSuit, string newRank)
{
	rank = newRank;
	suit = newSuit;
}

void Card::Display()
{
	cout << "A-H " << rank << " of " << suit << endl;
}

void Card::DisplayName()
{
	cout << rank << " of " << suit << endl;
}

string Card::GetRank()
{
	return rank;
}

char Card::GetSuit()
{
	return suit;
}