#include <iostream>
#include <string>
#include <fstream>
using namespace std;

//display two options: add new to-do item, or quit
void DisplayMAinMenu()
{
	cout << "1. New to-do item" << endl;
	cout << "2. Save and quit" << endl;
}

/*Get the user's input, validate whether it is betwee min and
max, if the value is invalid, ask the user to input it again 
(using a while loop). onve the user's input is valid, 
return that value*/
int GetChoice(int min, int max)
{
	cout << ">>";
	int userInput;
	cin >> userInput;

	while (userInput < min || userInput > max)
	{
		cout << "Invalid value, try again." << endl;
		cout << ">>";
		cin >> userInput;
	}
	return userInput;
}

//Get a line of text from the use, and return this info.
string GetToDoItem()
{
	cout << "\nPlease enter to-do item: ";
	string todoItem;
	cin.ignore();
	getline(cin, todoItem);
	return todoItem;
}

int main()
{
	ofstream output("todo.txt");
	int counter = 1;
	bool done = false;
	int menuChoice;
	string todoItem;

	cout << "TO DO LIST" << endl;

	while (!done)
	{
		cout << "\n---------------------------------" << endl;
		DisplayMAinMenu();
		menuChoice = GetChoice(1, 2);

		if (menuChoice == 1)
		{
			todoItem = GetToDoItem();
			output << counter << ". " << todoItem << endl;
			counter += 1;
		}
		else if (menuChoice == 2)
		{
			done = true;
		}
	}

	output.close();
	return 0;
}