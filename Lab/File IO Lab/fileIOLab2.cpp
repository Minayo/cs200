#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void DisplayMenu()
{
	cout << "\n---------------------------" << endl;
	cout << "Vote for a food item" << endl;
	cout << "1. Pizza" << endl;
	cout << "2. Samosas" << endl;
	cout << "3. Tacos" << endl;
	cout << "Bibimbap" << endl;

	cout << "\nOr quit the program with" << endl;
	cout << "5. Quit" << endl;
}

int GetVote(int min, int max)
{
	cout << "Choice: ";
	int userInput;
	cin >> userInput;

	while (userInput < min || userInput > max)
	{
		cout << "Invalid value, try again." << endl;
		cout << "Choice: ";
		cin >> userInput;
	}

	cout << "\tVote successful." << endl;
	cout << "\n(More....)" << endl;
	return userInput;
}

int main()
{
	ofstream output("foodVotes.txt");
	bool done = false;
	int vote;
	int pizza = 0;
	int samosas = 0;
	int tacos = 0;
	int bibimbap = 0;

	cout << "VOTING" << endl;

	while (!done)
	{
		DisplayMenu();
		vote = GetVote(1, 5);
		if (vote == 1) {pizza += 1;}
		else if (vote == 2) { samosas += 1; }
		else if (vote == 3) { tacos += 1; }
		else if (vote == 4) { bibimbap += 1; }
		else if (vote == 5) {done=true;}
	}
	output << "Pizza:    " << pizza << " votes" << endl;
	output << "Samosas:  " << samosas << " votes" << endl;
	output << "Tacos:    " << tacos << " votes" << endl;
	output << "Bibimbap: " << bibimbap << " votes" << endl;

	output.close();
	return 0;
}