#include <iostream>
#include <string>
using namespace std;

void Program1()
{
	cout << "Enter your hometown: ";
	string hometown;
	cin >> hometown;

	if (hometown.size() > 6) 
	{
		cout << "That's a long name!" << endl;
	}
	cout << "Enter your name: ";
	string name;
	cin >> name;

	cout << "Hello " << name << " from " << hometown << "!" << endl;
}

void Program2()
{
	cout << "How many points does the assignment have? ";
	float totalPoints;
	cin >> totalPoints;

	cout << "How many points did you get?              ";
	float userPoints;
	cin >> userPoints;

	float grade = (userPoints / totalPoints) * 100;
	cout << "Score:                                    " << grade << "%" << endl;

	if (grade >= 60) 
	{
		cout << "You passed!" << endl;
	}
	else { cout << "You failed!" << endl; }
}

void Program3()
{
	cout << "Enter your phone charge(%): ";
	int charge;
	cin >> charge;

	if (charge >= 75 && charge <= 100)
	{
		cout << "[****]" << endl;
	}

	else if (charge >= 50 && charge < 70)
	{
		cout << "[***_]" << endl;
	}

	else if (charge >= 25 && charge < 50)
	{
		cout << "[**__]" << endl;
	}

	else if (charge >= 5 && charge < 25)
	{
		cout << "[*___]" << endl;
	}

	else if (charge >= 0 && charge < 5)
	{
		cout << "[____]" << endl;
	}
}

void Program4()
{
	cout << "What is your favorite type of book?" << endl;
	cout << "1. Scifi" << endl;
	cout << "2. Historical" << endl;
	cout << "3. Fantasy" << endl;
	cout << "4. DIY" << endl;
	cout << "\nYour selection: ";
	int userChoice;
	cin >> userChoice;

	if (userChoice >= 1 && userChoice <= 4)
	{
		cout << "Good choice!" << endl;
	}
	else
	{
		cout << "Invalid choice!" << endl;
	}
}

void Program5()
{
	cout << "Enter first number:    ";
	float num1;
	cin >> num1;
	cout << "Enter second number:   ";
	float num2;
	cin >> num2;

	cout << "\nwhich type of operation?" << endl;
	cout << "+: add" << endl;
	cout << "-: subtract" << endl;
	cout << "*: nultiply" << endl;
	cout << "/: divide" << endl;
	cout << "\nChoice: ";
	char choice;
	cin >> choice;

	float result;

	switch (choice)
	{
		case '+':
			result = num1 + num2;
		break;

		case '-':
			result = num1 - num2;
		break;

		case '*':
			result = num1 * num2;
		break;

		case '/':
			result = num1 / num2;
			if (num2 == 0)
			{
				cout << "Can't divide by 0." << endl;
			}
		break;

		default:
			cout << "Invalid operation." << endl;
	}
	cout << "Result: " << result << endl;
}

int main()
{
	while (true)
	{
		cout << "Run witch program? (1-5): ";
		int choice;
		cin >> choice;

		cout << endl << endl;

		if (choice == 1) { Program1(); }
		else if (choice == 2) { Program2(); }
		else if (choice == 3) { Program3(); }
		else if (choice == 4) { Program4(); }
		else if (choice == 5) { Program5(); }

		cout << endl << "--------------------------------" << endl;
	}
	return 0;
}