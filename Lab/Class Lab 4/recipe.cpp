#include "recipe.h"
#include "ingredient.h"
#include <iostream>
#include <string>
using namespace std;


Recipe::Recipe()
{
	m_totalIngredients = 0;
}

void Recipe::SetName(string name)
{
	m_name = name;
}

void Recipe::SetInstructions(string instructions)
{
	m_instructions = instructions;
}

void Recipe::AddIngredienct(string name, float amount, string unit)
{
	if (m_totalIngredients == 10)
	{
		cout << "Ingredient list is full!" << endl;
		return;
	}
	m_ingredients[m_totalIngredients].Setup(name, amount, unit);
	m_totalIngredients++;
}

void Recipe::Display()
{
	cout << m_name << endl;
	for (int i = 0; i < m_totalIngredients; i++)
	{
		m_ingredients[i].Display();
	}
	cout << m_instructions << endl;
}