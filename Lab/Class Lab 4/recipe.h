#ifndef _RECIPE_HPP
#define _RECIPE_HPP

#include <string>
#include "ingredient.h"
using namespace std; 

class Recipe
{
public:
	Recipe();
	void SetName(string name);
	void SetInstructions(string instructions);
	void AddIngredienct(string name, float amount, string unit);
	void Display();

private:
	string m_name;
	string m_instructions;
	Ingredient m_ingredients[10];
	int m_totalIngredients;

};

#endif
