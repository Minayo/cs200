#include "ingredient.h"
#include <iostream>
#include <string>
using namespace std;

void Ingredient::Setup(string name, float amout, string unit)
{
	m_name = name;
	m_amout = amout;
	m_unit = unit;
}

void Ingredient::Display()
{
	cout << m_name << ": " << m_amout << m_unit << endl;
}