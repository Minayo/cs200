#include "parser.hpp"

vector<string> Split( string line, string delimiter )
{
    vector<string> fields;
    int indexOfDelim;

    do
    {
        indexOfDelim = line.find( delimiter );
        string partial = line.substr( 0, indexOfDelim );
        fields.push_back( partial );
        line = line.substr( indexOfDelim+1 );
    } while ( indexOfDelim != string::npos );

    // Final term
    fields.push_back( line );

    return fields;
}

void LoadCsvData( const string& filepath, vector<Animal>& animals )
{
    Timer timer;

    cout << "Loading file \"" << filepath << "..." << endl;
    timer.Start();

    ifstream input( filepath );

    if ( !input.good() )
    {
        cout << "Error loading \"" << filepath << "\"" << endl;
    }

    string buffer;
    bool firstLine = true;
    while ( getline( input, buffer ) )
    {
        vector<string> fields = Split( buffer, "," );

        if ( firstLine )
        {
            firstLine = false;
            continue;
        }

        Animal animal;
        animal.shelter = fields[0];
        animal.id = fields[1];
        animal.intakeDate = fields[2];
        animal.intakeType = fields[3];
        animal.intakeCondition = fields[4];
        animal.type = fields[5];
        animal.group = fields[6];
        animal.breed = fields[7] + "-" + fields[8];

        animals.push_back( animal );
    }

    cout << "* Done loading file (" << timer.GetElapsedMilliseconds() << " milliseconds)" << endl;
    cout << "* " << animals.size() << " records loaded" << endl;
}
