#include "search.hpp"

int LinearSearch( string findme, vector<Animal> arr )
{
    int size = arr.size();
    for ( int i = 0; i < size; i++ )
    {
        if ( arr[i].id == findme )
        {
            return i;
        }
    }

    return -1; // not found
}

int BinarySearch( string findme, vector<Animal> arr )
{
    int size = arr.size();
    int left = 0;
    int right = size - 1;

    while ( left <= right )
    {
        int mid = ( left + right ) / 2;

        if ( arr[mid].id < findme )
        {
            left = mid + 1;
        }
        else if ( arr[mid].id > findme )
        {
            right = mid - 1;
        }
        else if ( arr[mid].id == findme )
        {
            return mid;
        }
    }

    return -1; // not found
}
