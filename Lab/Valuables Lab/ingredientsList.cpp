#include <string>
#include <iostream>
using namespace std;

int main()
{
	float cupsOfButter = 1.0;
	float cupsOfSugar = 1.0;
	float eggs = 2;
	float tspsVanilla = 2;
	float tspsBakingSoda = 1;
	float tspsSalt = 0.5;
	float cupsFlour = 3;
	float cupsChocolateChips = 2;
	cout << cupsOfButter << " cups of butter" << endl;
	cout << cupsOfSugar << " cups of sugar" << endl;
	cout << eggs << " eggs" << endl;
	cout << tspsVanilla << " teaspoons vanilla" << endl;
	cout << tspsBakingSoda << " teaspoons of baking soda" << endl;
	cout << tspsSalt << " respoons salt" << endl;
	cout << cupsFlour << " cups of flour" << endl;
	cout << cupsChocolateChips << " cups of chocolate chips\n" << endl;

	float batchSize;
	cout << "How many batches?";
	cin >> batchSize;
	cupsOfButter = cupsOfButter * batchSize;
	cupsOfSugar = cupsOfSugar * batchSize;
	eggs = eggs * batchSize;
	tspsVanilla = tspsVanilla * batchSize;
	tspsBakingSoda = tspsBakingSoda * batchSize;
	tspsSalt = tspsSalt * batchSize;
	cupsFlour = cupsFlour * batchSize;
	cupsChocolateChips = cupsChocolateChips * batchSize;
	cout << "\nADJUSTED INGREDIENTS LIST" << endl;
	cout << cupsOfButter << " cups of butter" << endl;
	cout << cupsOfSugar << " cups of sugar" << endl;
	cout << eggs << " eggs" << endl;
	cout << tspsVanilla << " teaspoons vanilla" << endl;
	cout << tspsBakingSoda << " teaspoons of baking soda" << endl;
	cout << tspsSalt << " respoons salt" << endl;
	cout << cupsFlour << " cups of flour" << endl;
	cout << cupsChocolateChips << " cups of chocolate chips" << endl;



	return 0;
}