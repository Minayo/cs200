#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void Program1()
{
    const int MAX_COURSES = 6;
    int courseCount = 0;
    string myCourses[MAX_COURSES];

    cout << "\nCLASS LIST" << endl;
    cout << endl;
    for (int i = 0; i < MAX_COURSES; i++)
    {
        string userInput;
        cout << "Enter class " << i << " or type STOP : ";
        cin >> userInput;

        if (userInput == "STOP")
        {
            break;
        }
        else
        {
            myCourses[i] = userInput;
            courseCount += 1;
        }
    }

    cout << "\nYour class: " << endl;
    for (int j = 0; j < MAX_COURSES; j++)
    {
        cout << myCourses[j] << "  ";
    }
    cout << "\n" << endl;
}

void Program2()
{
    const int MAX_NUMBER = 5;
    string names[MAX_NUMBER];
    float prices[MAX_NUMBER];

    cout << "\nSTOREFRONT" << endl;
    for (int i = 0; i < MAX_NUMBER; i++)
    {
        cout << "\nITEM " << (i + 1) << endl;
        cout << "Enter item's name:   ";
        cin >> names[i];

        cout << "Enter item's price:  ";
        cin >> prices[i];
    }

    ofstream output("items.txt");
    output << "ITEMS FOR SALE" << endl;
    output << endl;
    for (int j = 0; j < MAX_NUMBER; j++)
    {
        output << j + 1 << ". " << names[j] << "  " << prices[j] << endl;
    }
    output.close();
    cout << endl;
}

void Program3()
{
    const string daysOfWeek[7] = 
    { 
        "Sunday", "Monday", "Tuesday", "Wednesday", 
        "Thursday", "Friday", "Saturday" 
    };

    const int DAYS_IN_WEEK = 7;
    const int HOURS_IN_DAY = 24;
    string todo[DAYS_IN_WEEK][HOURS_IN_DAY];
    int todoItemCount = 0;
    bool isDone = false;

    cout << "TO DO LIST" << endl;
    while (!isDone)
    {
        cout << "\n1. Add new to-do item" << endl;
        cout << "2. Save list" << endl;
        cout << "3. Quit" << endl;
        cout << "\n>>";
        int userChoice;
        cin >> userChoice;

        if (userChoice == 1)
        {
            cout << "\n0. Sunday      1. Monday" << endl;
            cout << "2. Tuesday     3. Wednesday" << endl;
            cout << "4. Thursday    5. Friday" << endl;
            cout << "6. Saturday" << endl;

            int dayOfWeek, timeOfDay;

            cout << "\nWhich day of thte week? (0 to 6):   ";
            cin >> dayOfWeek;
            cout << "\nWhat time of day? (0 to 23):        ";
            cin >> timeOfDay;
            cout << "\nWhat is the to do item?:            ";
            cin.ignore();
            getline(cin, todo[dayOfWeek][timeOfDay]);

            cout << "\nADDED" << endl;
        }
        else if (userChoice == 2)
        {
            string fileName;
            cout << "Enter the filename:  ";
            cin >> fileName;
            ofstream output(fileName);

            for (int day = 0; day < 7; day++)
            {
                output << daysOfWeek[day] << endl;

                for (int hour = 0; hour < 24; hour++)
                {
                    if (todo[day][hour] == "")
                        continue;
                    output << "\t"<< hour <<":00"<< "  " <<
                        todo[day][hour] << endl;
                }
            }
            output.close();
        }
        else
        {
            isDone = true;
        }
    }
    
}

int main()
{
    bool done = false;

    while ( !done )
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1" << endl;
        cout << "2. Program 2" << endl;
        cout << "3. Program 3" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
        }
    }
    
    return 0;
}
