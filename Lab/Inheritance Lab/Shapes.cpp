#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup(unit);
    m_width = width;
    m_height = height;
}

void Rectangle::DisplayArea()
{
    cout << this->CalculateArea() << " " << m_unit << endl;
}

float Rectangle::CalculateArea()
{
    float area = m_width * m_height;
    return area;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
    Shape::Setup(unit);
    m_radius = radius;
}

void Circle::DisplayArea()
{
    cout << this->CalculateArea() << " "<< m_unit <<  endl;
}

float Circle::CalculateArea()
{
    float sqradius = m_radius * m_radius;
    float area = 3.14 * sqradius;

    return area;
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
