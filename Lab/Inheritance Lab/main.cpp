#include <iostream>
using namespace std;

#include "GameProgram.hpp"
#include "ShapesProgram.hpp"

#include <ctime>
#include <cstdlib>

int main()
{
    srand( time( NULL ) ); // seed random # generator

    bool done = false;
    while ( !done )
    {
        cout << endl << endl << "----------- MAIN MENU -----------" << endl;
        cout << "1. Run Shapes" << endl;
        cout << "2. Run Game" << endl;
        cout << "3. Quit" << endl;
        cout << ">> ";
        int choice;
        cin >> choice;

        if ( choice == 1 )
        {
            ShapesProgram program;
            program.Run();
        }
        else if ( choice == 2 )
        {
            GameProgram program;
            program.Run();
        }
        else if ( choice == 3 )
        {
            done = true;
        }
    }

    return 0;
}
