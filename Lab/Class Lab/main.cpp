#include <iostream>
using namespace std;

#include "Die.h"
#include <ctime>
int main()
{
	srand(time(NULL));
	Die d20(20);
	int hitRoll = d20.Roll();

	Die d8(8);
	int damageRoll = d8.Roll();

	cout << hitRoll << " to hit, for " << damageRoll << " damage." << endl;
}