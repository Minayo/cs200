#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

float countChange(int quarters, int dimes, int nicles, int pennies)
{
	float quaterTotal = quarters * 0.25;
	float dimeTotal = dimes * 0.10;
	float nicleTotal = nicles * 0.05;
	float pennyTotal = pennies * 0.01;
	float total = quaterTotal + dimeTotal + nicleTotal + pennyTotal;
	return total;
}

int main()
{
	cout << "COUNT CHANGE" << endl;

	while (true)
	{
		int quarters, dimes, nicles, pennies;

		cout << "How many quaters? ";
		cin >> quarters;
		cout << "How many dimes?   ";
		cin >> dimes;
		cout << "How many nicles?  ";
		cin >> nicles;
		cout << "How many pennies? ";
		cin >> pennies;
		float total = countChange(quarters, dimes, nicles, pennies);

		cout << "\nYou have: $" << fixed <<setprecision(2)
			<< total << endl;

	}
	return 0;
}