#include <iostream>
#include <iomanip>
using namespace std;

void DisplayMenu()
{
	cout << "\n-------------------------" << endl;
	cout << "1. Add money" << endl;
	cout << "2. Substract money" << endl;
	cout << "3. Quit" << endl;
}

void PrintPrice(float price)
{
	if (price >= 0)
	{
		cout << "Result: $" << price;
	}
	else if (price < 0)
	{
		cout << "Result: -$" << -price;
	}
}

int main()
{
	cout << "MONEY MATH";

	while (true)
	{
		DisplayMenu();
		cout << ">>";
		int userChoice;
		cin >> userChoice;

		if (userChoice == 1)
		{
			cout << "\nEnter first amout: $";
			float num1;
			cin >> num1;

			cout << "Enter secound amout: $";
			float num2;
			cin >> num2;

			float sum = num1 + num2;
			PrintPrice(sum);
		}
		else if (userChoice == 2)
		{
			cout << "\nEnter first amout: $";
			float num1;
			cin >> num1;

			cout << "Enter secound amout: $";
			float num2;
			cin >> num2;
			float sum = num1 - num2;
			PrintPrice(sum);
		}
		else if (userChoice == 3)
		{
			cout << "\nGoodbye" << endl;
			break;
		}

	}
	return 0;
}