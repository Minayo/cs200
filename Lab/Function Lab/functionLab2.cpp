#include <iostream>
#include <iomanip>
using namespace std;
float AddTax(float dollar)
{
	float tax = dollar * 0.12;
	dollar = dollar + tax;
	return dollar;
}

int main()
{
	cout << "PRICE PLUS TAX" << endl;
	cout << "\nPrice: $" << 9.99
		<< "\twith tax: $ " << fixed <<setprecision(2)
		<< AddTax(9.99) << endl;

	cout << "Price: $" << 19.95
		<< "\twith tax: $ " << fixed << setprecision(2)
		<< AddTax(19.95) << endl;

	cout << "Price: $" << 10.00
		<< "\twith tax: $ " << fixed << setprecision(2)
		<< AddTax(10.00) << endl;
}