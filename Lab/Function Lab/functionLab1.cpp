#include <iostream>
using namespace std;

float percentToDecimal(float percent)
{
	float decimal = percent / 100;
	return decimal;
}

int main()
{
	float userPercent;
	float decimalVersion;
	cout << "PERCENT TO DECIMAL" << endl;
	cout << "\nEnter a percent value, without the %: ";
	cin >> userPercent;
	decimalVersion = percentToDecimal(userPercent);
	cout << "\nDecimal value: " << decimalVersion << endl;
	return 0;
}