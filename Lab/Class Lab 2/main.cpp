#include <iostream>
#include <string>
#include "student.h"

int main()
{
	Student students[10];
	for (int i = 0; i < 10; i++)
	{
		cout << "Enter Your name: ";
		string name;
		cin >> name;
		students[i].Setup(name);
		cout << "Enter your gpa: ";
		float gpa;
		cin >> gpa;
		students[i].AddGrade(gpa);
	}
	
	cout << "\nLIST OF STUDENTS AND GPA" << endl;
	for (int j = 0; j < 10; j++)
	{
		students[j].Display();
	}

	return 0;
}