#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    int count = 0;
    while (count < 21)
    {
        cout << count << " ";
        count += 1;
    }
}

void Program2()
{
    int count = 1;
    while (count < 129)
    {
        cout << count << " ";
        count *= 2;
    }
}

void Program3()
{
    int secretNumber = 374;
    int playerGuess;
    do
    {
        cout << "\nGuess a number: ";
        cin >> playerGuess;

        if (playerGuess > secretNumber)
        {
            cout << "Too high!" << endl;
        }
        else if (playerGuess < secretNumber)
        {
            cout << "Too low!" << endl;
        }
        else
        {
            cout << "That's right!" << endl;
        }
    } while (playerGuess != secretNumber);

    cout << "\nGAME OVER" << endl;
}

void Program4()
{
    cout << "Please enter a number betwee 1 and 5: ";
    int num;
    cin >> num;

    while (num < 1 || num > 5)
    {
        cout << "Invalid entry, try again: ";
        cin >> num;
    }

    cout << "Thank you." << endl;
}

void Program5()
{
    int yearCount = 0;
    cout << "What is your starting wage?            ";
    float startWage;
    cin >> startWage;
    cout << "What % rase do you get per year?       ";
    float percentRaisePerYear;
    cin >> percentRaisePerYear;
    cout << "How many years have you worked there?  ";
    int yearWorked;
    cin >> yearWorked;
    float adjustedWage = startWage;
    while (yearCount < yearWorked)
    {
        yearCount += 1;
        cout << "Salary at year " << yearCount << ":    " << adjustedWage << endl;
        adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage;
    }
}

void Program6()
{
    int counter = 1;
    int sum = 0;
    cout << "Enter a value for n: ";
    int n;
    cin >> n;

    while (counter <= n)
    {
        sum += counter;
        counter++;
        cout << "Sum: " << sum << endl;
    }
}

int main()
{
    // Don't modify main
    while (true)
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if (choice == 1) { Program1(); }
        else if (choice == 2) { Program2(); }
        else if (choice == 3) { Program3(); }
        else if (choice == 4) { Program4(); }
        else if (choice == 5) { Program5(); }
        else if (choice == 6) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
