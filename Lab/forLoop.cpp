#include <iostream>
#include <string>
using namespace std;

void Program1()
{
	for (int i = 0; i < 21; i++)
	{
		cout << i << " ";
	}
}

void Program2()
{
	for (int i = 1; i < 129; i *= 2)
	{
		cout << i << " ";
	}
}

void Program3()
{
	int sum = 0;
	cout << "Enter a value for n: ";
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		sum += i;
		cout << "Sum: " << sum << endl;
	}
}

void Program4()
{
	
	cout << "Enter a string: ";
	string text;
	cin.ignore();
	getline(cin, text);
	
	cout << "Enter a letter to count: ";
	char lookForLetter;
	cin >> lookForLetter;

	int letterCount = 0;
	for (int i = 0; i <= text.size(); i++)
	{
		cout << "Letter " << i << ": " << text[i] << endl;
		if (tolower(text[i]) == tolower(lookForLetter))
		{
			letterCount += 1;
		}
	}
	cout << "There are " << letterCount << " " << lookForLetter << "(s) in" << '"' << text << '"' << endl;
}

int main()
{    
	// Don't modify main    
	while ( true )    
	{        
		cout << "Run which program? (1-4): ";        
		int choice;        
		cin >> choice;        
		cout << endl << endl;        
		if      ( choice == 1 ) { Program1(); }        
		else if ( choice == 2 ) { Program2(); }        
		else if ( choice == 3 ) { Program3(); }        
		else if ( choice == 4 ) { Program4(); }        
		
		cout << endl << "------------------------------------" << endl;    
	}    
	return 0;
}